from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
import builtins
import json

@dataclass_json
@dataclass
class WialonMessage:
    reg: str = ''
    time: int = 0
    lng: float = 0
    lat: float = 0
    speed: float = 0
    course: int = 0
    float_params: list = field(default_factory=list)
    int_params: list = field(default_factory=list)
    string_params: list = field(default_factory=list)
    long_params: list = field(default_factory=list)
    bin_params: list = field(default_factory=list)
    files_params: list = field(default_factory=list)


class WialonMessagesParser(object):
    def __init__(self, messages=(), filename=None):
        self._messages = messages
        self.parsed_messages = []
        if filename:
            self.load(filename)

    def load(self, filename=''):
        try:
            with open(filename, 'r') as file:
                self._messages = file.readlines()
        except FileNotFoundError:
            raise WialonMessagesParserError(f"No such file : {filename}")

    @staticmethod
    def _convert_parameter_string_to_dict(param_string, value_type='str'):
        try:
            (key, value) = param_string.split(':')
            return {
                key: getattr(builtins, value_type)(value)
            }
        except ValueError:
            return {}

    @classmethod
    def _parse_params_string(cls, string, values_type='str'):
        return list(map(lambda param: cls._convert_parameter_string_to_dict(param,  values_type),
                   string.split(',')))

    @classmethod
    def _parse_message(cls, message):
        message_parts = message.split(';')
        return  WialonMessage(
                    reg=message_parts[0],
                    time=int(message_parts[1]),
                    lng=float(message_parts[2]),
                    lat=float(message_parts[3]),
                    speed=float(message_parts[4]),
                    course=int(message_parts[5]),
                    float_params=cls._parse_params_string(message_parts[6], values_type='float'),
                    int_params=cls._parse_params_string(message_parts[7].replace(',SATS', 'SATS'), values_type='int'),
                    string_params=cls._parse_params_string(message_parts[8]),
                    long_params=cls._parse_params_string(message_parts[9], values_type='int'),
                    bin_params = cls._parse_params_string(message_parts[10]),
                    files_params=cls._parse_params_string(message_parts[11])
                ).to_dict()

    def parse(self):
        self.parsed_messages = list(map(lambda message: self._parse_message(message), self._messages))
        return self.parsed_messages

    def to_json(self):
        return json.dumps(self.parsed_messages)

class WialonMessagesParserError(BaseException):
    ...
